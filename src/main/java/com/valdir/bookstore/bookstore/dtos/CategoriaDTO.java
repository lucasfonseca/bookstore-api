package com.valdir.bookstore.bookstore.dtos;

import com.valdir.bookstore.bookstore.domain.Categoria;
import org.hibernate.validator.constraints.Length;
import java.io.Serializable;
import javax.validation.constraints.NotEmpty;


public class CategoriaDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private Integer id;
  @NotEmpty(message = "Campo nome requerido")
  @Length(min = 3, max = 100, message = "O campo nome deve ter entre 3 e 100 caracteres")
  private String nome;

  @NotEmpty(message = "Campo descrição requerido")
  @Length(min = 3, max = 100, message = "O campo descrição deve ter entre 3 e 200 caracteres")
  private String descricao;


  public CategoriaDTO() {
    super();
  }

  /** . */
  public CategoriaDTO(Categoria obj) {
    super();
    this.id = obj.getId();
    this.nome = obj.getNome();
    this.descricao = obj.getDescricao();
  }

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNome() {
    return this.nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getDescricao() {
    return this.descricao;
  }

  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }



}

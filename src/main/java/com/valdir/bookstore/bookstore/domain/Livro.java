package com.valdir.bookstore.bookstore.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;

@Entity
public class Livro implements Serializable {

  private static final long serialVersionUID = 1L;

  // declaração do atribuitos
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @NotEmpty(message = "Campo título requerido")
  @Length(min = 3, max = 50, message = "O campo nome deve ter entre 3 e 50 caracteres")
  private String titulo;

  @NotEmpty(message = "Campo nome autor requerido")
  @Length(min = 3, max = 50, message = "O campo nome autor deve ter entre 3 e 50 caracteres")
  private String nomeautor;

  @NotEmpty(message = "Campo texto requerido")
  @Length(min = 10, max = 2000000,
      message = "O campo texto deve ter entre 10 e 2.000.000 caracteres")
  private String texto;

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "categoria_id")
  private Categoria categoria; // materialização do relacionamento


  public Livro() {
    super();
  }

  /**
   * Envolveu todos os itens. Se houvesse algum aarray list, este não entraria.
   */
  public Livro(Integer id, String titulo, String nomeautor, String texto, Categoria categoria) {
    super();
    this.id = id;
    this.titulo = titulo;
    this.nomeautor = nomeautor;
    this.texto = texto;
    this.categoria = categoria;
  }

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getTitulo() {
    return this.titulo;
  }

  public void setTitulo(String titulo) {
    this.titulo = titulo;
  }

  public String getNomeautor() {
    return this.nomeautor;
  }

  public void setNomeautor(String nomeautor) {
    this.nomeautor = nomeautor;
  }

  public String getTexto() {
    return this.texto;
  }

  public void setTexto(String texto) {
    this.texto = texto;
  }

  public Categoria getCategoria() {
    return this.categoria;
  }

  public void setCategoria(Categoria categoria) {
    this.categoria = categoria;
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (!(o instanceof Livro)) {
      return false;
    }
    Livro livro = (Livro) o;
    return Objects.equals(id, livro.id);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id);
  }


}

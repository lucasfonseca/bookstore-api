package com.valdir.bookstore.bookstore.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.Length;

@Entity
public class Categoria implements Serializable {

  private static final long serialVersionUID = 1L;
  // declaração do atribuitos
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @NotEmpty(message = "Campo nome requerido")
  @Length(min = 3, max = 100, message = "O campo nome deve ter entre 3 e 100 caracteres")
  private String nome;

  @NotEmpty(message = "Campo descrição requerido")
  @Length(min = 3, max = 100, message = "O campo descrição deve ter entre 3 e 200 caracteres")
  private String descricao;

  @OneToMany(mappedBy = "categoria")
  private List<Livro> livros = new ArrayList<>(); // materialização do relacionamento


  // declaração do construtor sem parametros
  public Categoria() {
    super();
  }


  /**
   * .declaração do construtor com parametros, exceto livros (pois né é necessário passar um array
   * de livros)
   */
  public Categoria(Integer id, String nome, String descricao) {
    super();
    this.id = id;
    this.nome = nome;
    this.descricao = descricao;
  }



  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNome() {
    return this.nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getDescricao() {
    return this.descricao;
  }

  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }

  public List<Livro> getLivros() {
    return this.livros;
  }

  public void setLivros(List<Livro> livros) {
    this.livros = livros;
  }


  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (!(o instanceof Categoria)) {
      return false;
    }
    Categoria categoria = (Categoria) o;
    return Objects.equals(id, categoria.id);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id);
  }

}

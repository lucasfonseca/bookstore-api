package com.valdir.bookstore.bookstore.service;

import com.valdir.bookstore.bookstore.domain.Categoria;
import com.valdir.bookstore.bookstore.domain.Livro;
import com.valdir.bookstore.bookstore.dtos.LivroDTO;
import com.valdir.bookstore.bookstore.repositories.LivroRepository;

import com.valdir.bookstore.bookstore.service.exceptions.ObjectNotFoundException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
public class LivroService {

  @Autowired
  private LivroRepository repository;


  @Autowired
  private CategoriaService categoriaService;

  /** . */
  public Livro findById(Integer id) {

    Optional<Livro> obj = repository.findById(id);
    return obj.orElseThrow(() -> new ObjectNotFoundException(
        "Objeto não encontrado! id: " + id + ", Tipo: " + Livro.class.getName()));

  }

  public List<Livro> findAll(Integer id_cat) {
    categoriaService.findById(id_cat);
    return repository.findAllByCategoria(id_cat);
  }

  public Livro create(Livro obj) {
    obj.setId(null);
    return repository.save(obj);
  }

  public Livro update(Integer id, Livro obj) {
    Livro newObj = findById(id);
    updateData(newObj, obj);
    return repository.save(newObj);
  }

  private void updateData(Livro newObj, Livro obj) {
    newObj.setTitulo(obj.getTitulo());
    newObj.setNomeautor(obj.getNomeautor());
    newObj.setTexto(obj.getTexto());

  }

  public void delete(Integer id) {
    Livro obj = findById(id);
    repository.delete(obj);
  }

  public Livro create(Integer id_cat, Livro obj) {
    obj.setId(null);
    Categoria cat = categoriaService.findById(id_cat);
    obj.setCategoria(cat);
    return repository.save(obj);
  }

}

package com.valdir.bookstore.bookstore.service;

import com.valdir.bookstore.bookstore.domain.Categoria;
import com.valdir.bookstore.bookstore.domain.Livro;
import com.valdir.bookstore.bookstore.repositories.CategoriaRepository;
import com.valdir.bookstore.bookstore.repositories.LivroRepository;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DBService {

  @Autowired
  private CategoriaRepository categoriaRepository;

  @Autowired
  private LivroRepository livroRepository;

  /** comentário java doc. */
  public void instanciaBaseDeDados() {

    Categoria cat1 = new Categoria(null, "Informática", "Livros de TI");
    Categoria cat2 = new Categoria(null, "Ficção Científica", "Livros de Nerd");
    Categoria cat3 = new Categoria(null, "Biografias", "Livros de Biografias");

    Livro l1 = new Livro(null, "Clean Code", "Robert Martin", "descrição1", cat1);
    Livro l2 = new Livro(null, "Engeharia de Software", "Louis V. Gerstner", "descrição2", cat1);
    Livro l3 = new Livro(null, "The Time Machine", "H.G. Wells", "descrição3", cat2);
    Livro l4 = new Livro(null, "The War of the Worlds", "H.G. Wells", "descrição4", cat2);
    Livro l5 = new Livro(null, "I, Robot", "Isaac Asimov", "descrição5", cat2);

    cat1.getLivros().addAll(Arrays.asList(l1, l2));
    cat2.getLivros().addAll(Arrays.asList(l3, l4, l5));

    categoriaRepository.saveAll(Arrays.asList(cat1, cat2, cat3));
    livroRepository.saveAll(Arrays.asList(l1, l2, l3, l4, l5));

  }

}

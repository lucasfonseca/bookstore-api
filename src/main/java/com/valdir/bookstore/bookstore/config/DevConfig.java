package com.valdir.bookstore.bookstore.config;

import com.valdir.bookstore.bookstore.service.DBService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("dev")
public class DevConfig {

  @Autowired
  private DBService dbService;

  @Value("${spring.jpa.hibernate.ddl-auto}")
  private String strategy;

  /** . */
  @Bean // inicia o método assim que a classe for instanciada
  public boolean instanciaBaseDeDados() {
    if (strategy.equals("create")) { // se a variavel strategy for igual a create
      this.dbService.instanciaBaseDeDados(); // chama o metodo para insntanciar os dados
    }
    return false;
  }

}
